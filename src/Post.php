<?php

namespace Webfeet\Facebook;

use DateTime;
use Facebook\GraphNodes\GraphEdge;
use Facebook\GraphNodes\GraphNode;

/**
 * Class FacebookPost
 */
class Post
{
    /**
     * @var string
     */
    public $id;

    /**
     * @var string
     */
    public $message;

    /**
     * @var array
     */
    public $images;

    /**
     * @var string
     */
    public $link;

    /**
     * @var DateTime
     */
    public $timestamp;

    /**
     * FacebookPost constructor.
     * @param GraphNode $graphNode
     */
    public function __construct($graphNode = null)
    {
        if ($graphNode) {
            $this->id = $graphNode->getField('id');
            $this->message = $graphNode->getField('message');
            $this->images = $this->_imagesFromAttachment($graphNode->getField('attachments'));
            $this->link = $graphNode->getField('permalink_url') ?: $graphNode->getField('link');
            $this->timestamp = $graphNode->getField('created_time');
        } else {
            $this->images = [];
            $this->timestamp = new DateTime();
        }
    }

    /**
     * @param GraphEdge $graphEdge
     * @return array
     */
    protected function _imagesFromAttachment($graphEdge)
    {
        $images = [];
        if ($graphEdge) {
            foreach ($graphEdge->all() as $graphNode) {
                /** @var GraphNode $graphNode */
                $type = $graphNode->getField('type');
                if (strpos($type, 'album') !== false) {
                    foreach ($graphNode->getField('subattachments') as $photoNode) {
                        $images[] = $this->_getImageUrl($photoNode);
                    }
                } elseif ($type == 'photo') {
                    $images[] = $this->_getImageUrl($graphNode);
                }
            }
        }
        return $images;
    }

    /**
     * @param GraphNode $graphNode
     * @return string
     */
    protected function _getImageUrl($graphNode)
    {
        /** @var GraphNode $imageData */
        $imageData = $graphNode->getField('media')->getField('image');
        return $imageData->getField('src');
    }

}