<?php

namespace Webfeet\Facebook;

use Exception;
use Facebook\Exceptions;
use Facebook\Facebook;
use Facebook\FacebookResponse;

/**
 * Class FacebookPost
 */
class Query
{
    /**
     * @var Facebook
     */
    protected $_fb;

    /**
     * Query constructor.
     * @param string $appId
     * @param string $appSecret
     * @param string $token
     */
    public function __construct(
        $appId,
        $appSecret,
        $token
    ){
        $this->_fb = new Facebook([
            'app_id' => $appId,
            'app_secret' => $appSecret,
            'default_graph_version' => 'v3.2',
            'default_access_token' => $token
        ]);
    }

    /**
     * @param string $query
     * @param string $token
     * @return FacebookResponse
     * @throws Exception
     */
    public function query($query, $token = null)
    {
        try {
            return $this->_fb->get($query, $token);
        } catch(Exceptions\FacebookResponseException $e) {
            throw new Exception('Graph returned an error: ' . $e->getMessage());
        } catch(Exceptions\FacebookSDKException $e) {
            throw new Exception('Facebook SDK returned an error: ' . $e->getMessage());
        }
    }

    /**
     * @param $page
     * @param string $token
     * @return Post[]
     */
    public function getBasicPostData($page, $token = null)
    {
        $endpoint = '/' . trim($page, '/') . '/posts';
        $params = [
            'fields' => implode(',', ['message', 'attachments', 'created_time', 'permalink_url', 'link']),
            'limit' => 5
        ];
        $query = $endpoint . '?' . http_build_query($params);

        $postData = [];
        try {
            $response = $this->query($query, $token);
            foreach ($response->getGraphEdge()->all() as $graphNode) {
                $postData[] = new Post($graphNode);
            }
        } catch (Exception $e) {
            $post = new Post();
            $post->message = 'Page "' . $page . '" - ' . $e->getMessage();
            $postData[] = $post;
        }

        return $postData;
    }

}